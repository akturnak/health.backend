from medicaments.models import DrugInfo


def links_to_id():
    drugs_analogs = DrugInfo.objects.exclude(analogs_links_json=[])
    for drug in drugs_analogs:
        for link in drug.analogs_links_json:
            try:
                analog = DrugInfo.objects.get(source_link=link)
                drug.analogs.add(analog)
            except:
                print("can't find link: " + link + " drug: " + drug.source_link)
        drug.save()
