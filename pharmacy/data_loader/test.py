class Fighter(object):
    def __init__(self, name, health, damage_per_attack):
        self.name = name
        self.health = health
        self.damage_per_attack = damage_per_attack


def declare_winner(fighter1, fighter2, first_attacker):
    # name, health, damage
    if fighter1.health <= 0:
        return fighter2.name
    if fighter2.health <= 0:
        return fighter1.name
    if first_attacker == fighter1.name:
        fighter2.health -= fighter1.damage_per_attack
        return declare_winner(fighter1, fighter2, fighter2.name)
    else:
        fighter1.health -= fighter2.damage_per_attack
        return declare_winner(fighter1, fighter2, fighter1.name)


print(declare_winner(Fighter("Lew", 10, 2), Fighter("Harry", 5, 4), "Lew"))
