from bs4 import BeautifulSoup

text = open('/home/nomad/projects/web/health/data_loader/website_parser/files/1491201862.851234%s' % '.html', 'r')

# Beautiful Soup
soup = BeautifulSoup(text, "html.parser")

brand_name = soup.find('h1', {'class': 'page-info__title'}).text

release_form = soup.find('div', {'class': 'page-info__title page-info__title_form'}).text.split(',')[1].strip()

eng_name = soup.find('div', {'class': 'page-info__subtitle margin_bottom_20'}).text
# Значки противопоказаний
contradictions_icons = soup.find('div', {'class': 'page-info__indications margin_top_20'}) \
    .find('div', {'class': 'page-info__indications margin_top_20'})
# Аналоги
links = soup.find('div', {'class': 'columns columns_percent js-analog__list'}) \
    .findAll('a', {'class': 'entry__link link-holder'})
analogs_links = list(map(lambda x: x.get('href'), links))
# Клинико-фармакологическая группа
pharmacologic_group = soup.find('h2', {'id': 'pharmacologic_group'}).findNext('div').text
# Форма выпуска, состав и упаковка
composition = soup.find('h2', {'id': 'composition'}).findNext('div').text
# Показания
indication = soup.find('h2', {'id': 'indication'}).findNext('div').text
# Противопоказания
contraindications = soup.find('h2', {'id': 'contraindications'}).findNext('div').text
# Дозировка
dosage = soup.find('h2', {'id': 'dosage'}).findNext('div').text
# Побочные действия
side_effects = soup.find('h2', {'id': 'side_effects'}).findNext('div').text
# Передозировка
overdosage = soup.find('h2', {'id': 'overdosage'}).findNext('div').text
# Лекарственное взаимодействие
interaction = soup.find('h2', {'id': 'interaction'}).findNext('div').text
# Особые указания
special_instructions = soup.find('h2', {'id': 'special_instructions'}).findNext('div').text
# Беременность и лактация
pregnancy_contraindications_description = soup.find('h2', {'id': 'pregnancy_contraindications_description'}).findNext(
    'div').text
# Применение в детском возрасте
child_contraindications_description = soup.find('h2', {'id': 'child_contraindications_description'}).findNext(
    'div').text
# При нарушениях функции почек
renal_contraindications_description = soup.find('h2', {'id': 'renal_contraindications_description'}).findNext(
    'div').text
# При нарушениях функции печени
hepato_contraindications_description = soup.find('h2', {'id': 'hepato_contraindications_description'}).findNext(
    'div').text
# Условия отпуска из аптек
supply_conditions = soup.find('h2', {'id': 'supply_conditions'}).findNext(
    'div').text
# Условия и сроки хранения
storage_condition = soup.find('h2', {'id': 'storage_condition'}).findNext(
    'div').text

print(analogs_links)

# Beatiful Soup
# movie_link = item.find('div', {'class': 'nameRus'}).find('a').get('href')
# movie_desc = item.find('div', {'class': 'nameRus'}).find('a').text
# ['icon_medicament_pregnancy': при беременности:, 'icon_medicament_lactation': при кормлении грудью:
# , 'icon_medicament_child': в детском возрасте , 'icon_medicament_hepato': при нарушениях функции печени
# , 'icon_medicament_renal': при нарушении функции почек, 'icon_medicament_eldery': в пожилом возрасте]
# color_green_lighter: Можно
# color_red: Противопоказан
# color_yellow_light: С осторожностью
# [<span class="catalog__item__title">Клинико-фармакологическая группа</span>,
# <span class="catalog__item__title">Форма выпуска, состав и упаковка</span>,
# <span class="catalog__item__title">Показания</span>,
# <span class="catalog__item__title">Противопоказания</span>,
# <span class="catalog__item__title">Дозировка</span>,
# <span class="catalog__item__title">Побочные действия</span>,
#  <span class="catalog__item__title">Передозировка</span>,
#  <span class="catalog__item__title">Лекарственное взаимодействие</span>,
# <span class="catalog__item__title">Особые указания</span>,
#  <span class="catalog__item__title">Беременность и лактация</span>,
# <span class="catalog__item__title">Применение в детском возрасте</span>,
# <span class="catalog__item__title">При нарушениях функции почек</span>,
# <span class="catalog__item__title">При нарушениях функции печени</span>,
# <span class="catalog__item__title">Применение в пожилом возрасте</span>,
# <span class="catalog__item__title">Условия отпуска из аптек</span>,
# <span class="catalog__item__title">Условия и сроки хранения</span>,
# <span class="catalog__item__title">Фармакологическое действие</span>,
# <span class="catalog__item__title">Фармакокинетика</span>]
