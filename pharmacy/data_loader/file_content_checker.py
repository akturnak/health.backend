from os import listdir
from os.path import isfile, join

from bs4 import BeautifulSoup

my_path = '/home/nomad/projects/web/health/data_loader/website_parser/files/'
files = [join(my_path, f) for f in listdir(my_path) if isfile(join(my_path, f))]

list_paragraphs = []

for f in files:
    with open(f, 'r') as output_file:
        text = output_file.read()
        soup = BeautifulSoup(text, "html.parser")
        paragraphs = soup.find('div', {'class': 'columns columns_percent'}) \
            .findAll('span', {'class': 'catalog__item__title'})
        for paragraph in paragraphs:
            if paragraph not in list_paragraphs:
                list_paragraphs.append({paragraph: paragraph.parent.get('href')})
        if len(list_paragraphs) == 18:
            break

print(str(len(list_paragraphs)))
print(list_paragraphs)
