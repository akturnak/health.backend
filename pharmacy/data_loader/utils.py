import json
from os.path import join

import xlrd
from bs4 import BeautifulSoup

from app.medicaments.models import GovDrug, DrugInfo, DrugType, DrugCatalog

REG_CERT = 0
BRAND_NAME = 7
GENERIC_NAME = 9
RELEASE_FORM = 10
BARCODE = 12
PHARM_GROUP = 14
ICON_STYLES_LIST = ['icon', 'icon_medicament', 'margin_right_10']
FILES_PATH = '/home/nomad/projects/web/health/data_loader/website_parser/files/'


def registry_sync():
    """
    TODO: Не загружать последние строчки с датой документа('29.03.2017 22:01:36')
    возможно фильтровать регуляркой
    :return: 
    """
    rb = xlrd.open_workbook('/home/nomad/projects/web/health/data_loader/grls2017-03-29-1.xls')
    sheet = rb.sheet_by_index(0)
    for row_num in range(2, sheet.nrows):
        row = sheet.row_values(row_num)
        reg_cert = row[REG_CERT].strip()
        if not reg_cert:
            continue
        brand_name = row[BRAND_NAME].strip()
        generic_name = row[GENERIC_NAME].strip()
        dirty_barcode = row[BARCODE].strip()
        barcode = dirty_barcode.split(',')[0]
        release_form = row[RELEASE_FORM].strip()
        pharm_group = row[PHARM_GROUP].strip()
        GovDrug.objects.create(brand_name=brand_name, generic_name=generic_name,
                               barcode=barcode, reg_cert=reg_cert,
                               release_form=release_form, pharm_group=pharm_group)


def create_drug_object(file_name, url, drug_type):
    """
    Создает запись с информацией о лекарстве из распарсенных данных.
    :return: 
    """
    with open(file_name, 'r') as output_file:
        text = output_file.read()
        soup = BeautifulSoup(text, "html.parser")

        brand_name = soup.find('h1', {'class': 'page-info__title'}).text
        eng_name = soup.find('div', {'class': 'page-info__subtitle'}).text

        release_form_raw = soup.find('div', {'class': 'page-info__title page-info__title_form'})
        if release_form_raw:
            release_form = release_form_raw.text.split(',')[1].strip()
        else:
            release_form = None

        contradictions_icons = []
        contradictions_icons_raw = soup.find('div', {'class': 'page-info__indications margin_top_20'})
        if contradictions_icons_raw:
            contradictions_icons_raw = list(map(lambda x: x.get('class'), contradictions_icons_raw
                                                .findAll('a', {})))
            for contradiction in contradictions_icons_raw:
                contradiction = [x for x in contradiction if x not in ICON_STYLES_LIST]
                contradictions_icons.append(contradiction)

        links_raw = soup.find('div', {'class': 'columns columns_percent js-analog__list'})
        if links_raw:
            analogs_links = list(
                map(lambda x: x.get('href'), links_raw.findAll('a', {'class': 'entry__link link-holder'})))
        else:
            analogs_links = []

        pharmacologic_group = soup.find('h2', {'id': 'pharmacologic_group'})
        if pharmacologic_group:
            pharmacologic_group = pharmacologic_group.findNext('div').text

        composition = soup.find('h2', {'id': 'composition'})
        if composition:
            composition = composition.findNext('div').text

        indication = soup.find('h2', {'id': 'indication'})
        if indication:
            indication = indication.findNext('div').text

        contraindications = soup.find('h2', {'id': 'contraindications'})
        if contraindications:
            contraindications = contraindications.findNext('div').text

        dosage = soup.find('h2', {'id': 'dosage'})
        if dosage:
            dosage = dosage.findNext('div').text

        side_effects = soup.find('h2', {'id': 'side_effects'})
        if side_effects:
            side_effects = side_effects.findNext('div').text

        overdosage = soup.find('h2', {'id': 'overdosage'})
        if overdosage:
            overdosage = overdosage.findNext('div').text

        interaction = soup.find('h2', {'id': 'interaction'})
        if interaction:
            interaction = interaction.findNext('div').text

        special_instructions = soup.find('h2', {'id': 'special_instructions'})
        if special_instructions:
            special_instructions = special_instructions.findNext('div').text

        pregnancy = soup.find('h2', {'id': 'pregnancy_contraindications_description'})
        if pregnancy:
            pregnancy = pregnancy.findNext('div').text

        child = soup.find('h2', {'id': 'child_contraindications_description'})
        if child:
            child = child.findNext('div').text

        renal = soup.find('h2', {'id': 'renal_contraindications_description'})
        if renal:
            renal = renal.findNext('div').text

        hepato = soup.find('h2', {'id': 'hepato_contraindications_description'})
        if hepato:
            hepato = hepato.findNext('div').text

        supply = soup.find('h2', {'id': 'supply_conditions'})
        if supply:
            supply = supply.findNext('div').text

        storage = soup.find('h2', {'id': 'storage_condition'})
        if storage:
            storage = storage.findNext('div').text

        DrugInfo.objects.create(brand_name=brand_name, eng_name=eng_name, drug_type=drug_type,
                                release_form=release_form, source_link=url,
                                contradictions_icons_json=contradictions_icons, analogs_links_json=analogs_links,
                                pharmacologic_group=pharmacologic_group, composition=composition,
                                indication=indication, contraindications=contraindications, dosage=dosage,
                                side_effects=side_effects, overdosage=overdosage, interaction=interaction,
                                special_instructions=special_instructions, pregnancy=pregnancy, child=child,
                                renal=renal, hepato=hepato, supply=supply, storage=storage)


def load_catalog_info():
    with open('/home/nomad/projects/web/health/data_loader/website_parser/local_files_links.json') as file:
        data = json.load(file)
    for catalog in data:
        catalog_object = DrugCatalog.objects.create(name=catalog['title'])
        for drug_type in catalog['urls']:
            drug_type_object = DrugType.objects.create(name=drug_type[0],
                                                       catalog=catalog_object,
                                                       link=drug_type[1])
            for drug in drug_type[2]:
                for i in drug:
                    drug_link = i
                    drug_file = '%s.html' % drug[i]
                    create_drug_object(join(FILES_PATH, drug_file), drug_link, drug_type_object)
                    # break
                    # break


def clear_prev_load():
    DrugInfo.objects.all().delete()
    DrugType.objects.all().delete()
    DrugCatalog.objects.all().delete()


def print_consistency():
    print("Drugs count %s" % DrugInfo.objects.all().count())
    print("DrugTypes count %s" % DrugType.objects.all().count())
    print("DrugCatalogs items %s" % DrugCatalog.objects.all().count())




# clear_prev_load()
# load_catalog_info()
# print_consistency()
