from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class GovDrug(models.Model):
    """
    
    Класс описания лекарственного препарата.
    Базируется на Государственном реестре лекарственных средств РФ.(https://grls.rosminzdrav.ru/GRLS.aspx)
    
    Аттрибуты:
        brand_name: Торговое наименование лекарственного препарата
        generic_name: Международное непатентованное (МНН) или химическое наименование
        barcode: Штрих-код потребительской упаковки (EAN-13),
                длинна 14 символов из-за загрузки из гос. реестра 
        reg_cert: Номер регистрационного удостоверения
        release_form: Форма выпуска
        pharm_group: Фармакотерапевтическая группа
        
    """
    brand_name = models.CharField(max_length=500)
    generic_name = models.CharField(max_length=800)
    barcode = models.CharField(max_length=14)
    reg_cert = models.CharField(max_length=27)
    release_form = models.CharField(max_length=500)
    pharm_group = models.CharField(max_length=250)


class DrugCatalog(models.Model):
    """
    
    Класс рубрик каталога видов лекарств.
    Например: Пищеварительный тракт и обмен веществ, Кроветворение и кровь
    
    """
    name = models.CharField(max_length=250)


class DrugType(models.Model):
    """

    Класс вида лекарственного препарата.
    Входит в класс DrugCatalog
    Например: Стоматологические препараты, Иммуноcтимуляторы, Витамины

    """
    name = models.CharField(max_length=300)
    catalog = models.ForeignKey(DrugCatalog)
    link = models.CharField(max_length=500)


class DrugInfo(models.Model):
    """

    Класс инструкции лекарственного препарата.
    Базируется на информации из открытых источников

    Аттрибуты:
        brand_name: Торговое наименование лекарственного препарата
        generic_name: Международное непатентованное (МНН) или химическое наименование
        barcode: Штрих-код потребительской упаковки (EAN-13),
                длинна 14 символов из-за загрузки из гос. реестра 
        reg_cert: Номер регистрационного удостоверения
        release_form: Форма выпуска
        pharm_group: Фармакотерапевтическая группа

    """
    brand_name = models.CharField(max_length=500)
    eng_name = models.CharField(max_length=500, null=True)
    drug_type = models.ForeignKey(DrugType, null=True)
    release_form = models.CharField(max_length=100, null=True)
    source_link = models.TextField()

    contradictions_icons_json = JSONField(null=True)
    analogs_links_json = JSONField(null=True)

    analogs = models.ManyToManyField("self", blank=True)

    pharmacologic_group = models.CharField(max_length=500, null=True)
    composition = models.TextField(null=True)
    indication = models.TextField(null=True)
    contraindications = models.TextField(null=True)
    dosage = models.TextField(null=True)
    side_effects = models.TextField(null=True)
    overdosage = models.TextField(null=True)
    interaction = models.TextField(null=True)
    special_instructions = models.TextField(null=True)
    pregnancy = models.TextField(null=True)
    child = models.TextField(null=True)
    renal = models.TextField(null=True)
    hepato = models.TextField(null=True)
    supply = models.TextField(null=True)
    storage = models.TextField(null=True)
