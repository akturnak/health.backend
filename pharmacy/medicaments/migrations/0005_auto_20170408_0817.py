# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-08 08:17
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medicaments', '0004_auto_20170408_0728'),
    ]

    operations = [
        migrations.AlterField(
            model_name='druginfo',
            name='analogs_links_json',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='druginfo',
            name='contradictions_icons_json',
            field=django.contrib.postgres.fields.jsonb.JSONField(null=True),
        ),
        migrations.AlterField(
            model_name='druginfo',
            name='eng_name',
            field=models.CharField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='druginfo',
            name='release_form',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
