# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-17 05:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medicaments', '0005_auto_20170408_0817'),
    ]

    operations = [
        migrations.AddField(
            model_name='druginfo',
            name='child',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='composition',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='contraindications',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='dosage',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='hepato',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='indication',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='interaction',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='overdosage',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='pharmacologic_group',
            field=models.CharField(max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='pregnancy',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='renal',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='side_effects',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='special_instructions',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='storage',
            field=models.TextField(null=True),
        ),
        migrations.AddField(
            model_name='druginfo',
            name='supply',
            field=models.TextField(null=True),
        ),
    ]
