# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-08 07:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('medicaments', '0003_auto_20170406_0743'),
    ]

    operations = [
        migrations.CreateModel(
            name='DrugCatalog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='DrugType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('link', models.CharField(max_length=500)),
                ('catalog', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='medicaments.DrugCatalog')),
            ],
        ),
        migrations.AddField(
            model_name='druginfo',
            name='drug_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='medicaments.DrugType'),
        ),
    ]
