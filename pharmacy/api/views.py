from django.core.exceptions import ObjectDoesNotExist

from rest_framework.decorators import api_view
from rest_framework.response import Response

from medicaments.models import DrugInfo, GovDrug
from .serializers import DrugSerializer, DrugSuggestSerializer


@api_view(['GET'])
def drug_by_name(request):
    """
    Функция поиска лекарств.
    :param request: 
    :return: 
    """
    drug_name = request.query_params.get('name', None)
    barcode = request.query_params.get('barcode', None)
    search = request.query_params.get('search', None)

    if drug_name:  # если указано ищем по названию
        drugs = DrugInfo.objects.filter(brand_name__iexact=drug_name)  # todo: is sql-injection safe?
        if not drugs.count():
            return Response(status=404)
        serializer = DrugSerializer(drugs, many=True)
        content = {'drugs': serializer.data}
        return Response(content)

    elif barcode:  # если указан штрихкод ищем по нему
        drugs_from_registry = GovDrug.objects.filter(barcode=barcode)
        if not drugs_from_registry.count():
            return Response(status=404)
        for drug_name in drugs_from_registry:
            drugs = DrugInfo.objects.filter(brand_name__iexact=drug_name.brand_name)
            if drugs.count():
                serializer = DrugSuggestSerializer(drugs, many=True)
                content = {'drug_suggestions': serializer.data}
                return Response(content)
        return Response(status=404)

    elif search and len(search) > 2:
        drugs = DrugInfo.objects.filter(brand_name__istartswith=search).order_by('brand_name')[:20]
        if not drugs.count():
            return Response(status=404)
        serializer = DrugSuggestSerializer(drugs, many=True)
        content = {'drug_suggestions': serializer.data}
        return Response(content)

    else:  # если не указаны параметры поиска - возвращаем ошибку
        return Response(status=400)


@api_view(['GET'])
def drug_by_id(request, drug_id):
    try:
        drug = DrugInfo.objects.get(id=drug_id)  # todo: validate for integer
    except ObjectDoesNotExist:
        return Response(status=404)
    serializer = DrugSerializer(drug)
    return Response({'drug': serializer.data})


@api_view(['GET'])
def news(request):
    pass


@api_view(['GET'])
def drug_search(request):
    """
    Поиск лекарств.
    :param request: 
    :return: 
    """
    name = request.query_params.get('name', None)  # название препарата
    barcode = request.query_params.get('barcode', None)  # штрихкод препарата

    if barcode:
        registry_drugs = GovDrug.objects.filter(barcode=barcode)
        if registry_drugs.count():
            for name in registry_drugs:
                drugs = DrugInfo.objects.filter(brand_name__iexact=name.brand_name)
                if drugs.count():
                    serializer = DrugSuggestSerializer(drugs, many=True)
                    return Response({'drug_suggestions': serializer.data})

    elif name and len(name) > 2:
        drugs = DrugInfo.objects.filter(brand_name__istartswith=name).order_by('brand_name')[:20]
        if drugs.count():
            serializer = DrugSuggestSerializer(drugs, many=True)
            return Response({'drug_suggestions': serializer.data})

    return Response(status=404)
