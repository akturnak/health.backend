from rest_framework import status
from rest_framework.test import APITestCase

from medicaments.models import DrugInfo, DrugType, DrugCatalog, GovDrug


class DrugTest(APITestCase):
    def setUp(self):
        # создаем каталог и рубрики для лекарства
        drug_catalog = DrugCatalog.objects.create(name='Противомикробные препараты для системного применения')
        drug_type = DrugType.objects.create(name='Противовирусные препараты для системного применения',
                                            catalog=drug_catalog,
                                            link='/drug/kagocel/')
        # создаем лекарство
        brand_name = 'КАГОЦЕЛ'
        eng_name = 'KAGOCEL'
        DrugInfo.objects.create(brand_name=brand_name, eng_name=eng_name, drug_type=drug_type)

        # создаем запись из гос. реестра лек. препаратов
        # brand_name = models.CharField(max_length=500)
        # generic_name = models.CharField(max_length=800)
        # barcode = models.CharField(max_length=14)
        # reg_cert = models.CharField(max_length=27)
        # release_form = models.CharField(max_length=500)
        # pharm_group = models.CharField(max_length=250)
        GovDrug.objects.create(brand_name="Кагоцел", generic_name="-", barcode="4605340000312",
                               reg_cert="", release_form="", pharm_group="")

    def test_get_by_name(self):
        response = self.client.get('/api/v1/drugs', {'name': 'Кагоцел'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # response.data
        # self.assertEquals()

    def test_get_by_barcode(self):
        response = self.client.get('/api/v1/drugs', {'barcode': 4605340000312})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)
