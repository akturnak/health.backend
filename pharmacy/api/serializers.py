from rest_framework import serializers

from medicaments.models import DrugInfo


class DrugSuggestSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrugInfo
        fields = ('id', 'brand_name', 'release_form')


class DrugSerializer(serializers.ModelSerializer):
    analogs = DrugSuggestSerializer(many=True)

    class Meta:
        model = DrugInfo
        fields = ('id', 'brand_name', 'eng_name', 'drug_type', 'release_form', 'pharmacologic_group',
                  'contradictions_icons_json', 'analogs',
                  'composition', 'indication', 'contraindications', 'dosage', 'side_effects', 'overdosage',
                  'interaction', 'special_instructions', 'pregnancy', 'child', 'renal', 'hepato', 'supply', 'storage')
