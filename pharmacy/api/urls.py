from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^drugs$', views.drug_by_name),
    url(r'^drugs/(?P<drug_id>[\w]+)$', views.drug_by_id),
    url(r'^search/drugs$', views.drug_search),
    # url(r'^posts/last/date$', views.get_last_post_date),
    # url(r'^feed$', views.get_feed),
    # url(r'^posts/fresh$', views.get_feed),
    # url(r'^posts/top$', views.get_top),
    # url(r'^posts/(?P<post_id>[\w]+)/likes/add$', views.post_add_like),
    # url(r'^posts/(?P<post_id>[\w]+)/likes/delete$', views.post_delete_like),
    # url(r'^users/favorites$', views.get_favorite),
    # url(r'^auth/access_token$', views.get_token),
    # url(r'^dev/generate_post$', views.dev_generate_post),
]
