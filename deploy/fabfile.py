#!python
from fabric.api import run, sudo, put, cd, settings, env
from fabric.api import *

env.use_ssh_config = True


def test():
    env.hosts = ['test.fitubody.com', ]


def stage():
    env.hosts = ['stage.fitubody.com', ]


def production():
    env.hosts = ['fitubody.com', ]


def server_setup():
    # locale settings - check it!
    sudo('update-locale')
    sudo('localedef ru_RU.UTF-8 -i ru_RU -fUTF-8')
    # set server timezone, setup auto ntp update for date
    sudo('cp /usr/share/zoneinfo/Asia/Yekaterinburg /etc/localtime')
    sudo('apt-get -y install ntpdate')
    # sudo('ntpdate us.pool.ntp.org') # doesn't work on OpenVZ'
    sudo('apt-get -y install ntp')
    #
    sudo('apt-get -y install git')
    sudo('apt-get -y install nginx')
    sudo('apt-get -y install postgresql-9.4')
    sudo('apt-get -y install redis-server')
    sudo('apt-get -y install libpq-dev')
    sudo('apt-get -y install ca-certificates')
    run('wget https://bootstrap.pypa.io/get-pip.py')
    sudo('python get-pip.py')
    sudo('rm get-pip.py')
    sudo('pip install virtualenv')
    sudo('apt-get -y install python3.4')
    sudo('apt-get -y install python3-dev')
    sudo('apt-get -y install gcc')
    with cd('.ssh/'):
        put('id_rsa', 'id_rsa', mode=0600)
        put('id_rsa.pub', 'id_rsa.pub', mode=0600)


# @parallel
def deploy():
    project_dir = '/home/nomad/projects/pharmacy_project'
    virt_env_dir = '/home/nomad/virtualenvs/pharmacy'
    with settings(warn_only=True):
        if run("test -d %s" % project_dir).failed:
            run('mkdir -p %s' % project_dir)
            run('ssh -oStrictHostKeyChecking=no $h bitbucket.org')
            run("git clone git@bitbucket.org:arcodeca-dev-mobile/health.backend.git %s" % project_dir)
            run('virtualenv -p python3 %s' % virt_env_dir)
            run('source %s/bin/activate && pip install -r %s/requirements.txt'
                % (virt_env_dir, project_dir))

            with settings(sudo_user='postgres'):
                with open("initial_psql_db_setup.sql") as f:
                    for sql_cmd in f:
                        sudo('psql -c "%s"' % sql_cmd)

            with cd(project_dir):
                put('local_settings.py', 'pharmacy/config/local_settings.py')
                run('%s/bin/python pharmacy/manage.py migrate' % virt_env_dir)

            with cd('%s/pharmacy/' % project_dir):
                put('uwsgi.ini', 'uwsgi.ini')
                put('uwsgi_params', 'uwsgi_params')
            put('uwsgi.service', '/etc/systemd/system/uwsgi.service', use_sudo=True)
            sudo('systemctl enable uwsgi.service')
            sudo('systemctl start uwsgi.service')

            put('pharmacy_nginx.conf', '/etc/nginx/sites-available/pharmacy_nginx.conf', use_sudo=True)
            sudo('ln -s /etc/nginx/sites-available/pharmacy_nginx.conf /etc/nginx/sites-enabled/pharmacy_nginx.conf')
            sudo('systemctl stop nginx.service')
            sudo('systemctl start nginx.service')

    with cd(project_dir):
        run("git pull")
        run('source %s/bin/activate && pip install -r requirements.txt' % virt_env_dir)
        put('local_settings.py', 'pharmacy/config/local_settings.py')
        run('%s/bin/python pharmacy/manage.py migrate' % virt_env_dir)
        run("touch pharmacy/wsgi.reload")


def load_dump():
    put('pharmacy_db.backup', '/tmp/pharmacy_db.backup')
    with settings(sudo_user='postgres'):
        sudo('psql -c "DROP DATABASE pharmacy_db;"')
        sudo('psql -c "CREATE DATABASE pharmacy_db;"')
        sudo('pg_restore -d pharmacy_db /tmp/pharmacy_db.backup')
